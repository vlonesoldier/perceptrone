const zero = [1, -1, -1, -1, -1, -1,
    -1, 1, 1, 1, -1,
    -1, 1, -1, 1, -1,
    -1, 1, -1, 1, -1,
    -1, 1, -1, 1, -1,
    -1, 1, 1, 1, -1,
    -1, -1, -1, -1, -1],
    one = [1, -1, -1, -1, -1, -1,
        -1, 1, 1, -1, -1,
        -1, -1, 1, -1, -1,
        -1, -1, 1, -1, -1,
        -1, -1, 1, -1, -1,
        -1, -1, 1, -1, -1,
        -1, -1, -1, -1, -1],
    two = [1, -1, -1, -1, -1, -1,
        -1, 1, 1, 1, -1,
        -1, 1, -1, 1, -1,
        -1, -1, -1, 1, -1,
        -1, 1, 1, 1, -1,
        -1, 1, -1, -1, -1,
        -1, 1, 1, 1, -1],
    three = [1, -1, -1, -1, -1, -1,
        -1, 1, 1, 1, -1,
        -1, -1, -1, 1, -1,
        -1, -1, 1, -1, -1,
        -1, -1, -1, 1, -1,
        -1, 1, -1, -1, -1,
        -1, 1, 1, 1, -1],
    four = [1, -1, -1, -1, -1, -1,
        -1, 1, -1, 1, -1,
        -1, 1, -1, 1, -1,
        -1, 1, 1, 1, -1,
        -1, -1, -1, 1, -1,
        -1, -1, -1, 1, -1,
        -1, -1, -1, -1, -1],
    five = [1, -1, -1, -1, -1, -1,
        -1, 1, 1, 1, -1,
        -1, 1, -1, -1, -1,
        -1, 1, 1, 1, -1,
        -1, -1, -1, 1, -1,
        -1, 1, 1, 1, -1,
        -1, -1, -1, -1, -1],
    six = [1, -1, -1, -1, -1, -1,
        -1, 1, 1, 1, -1,
        -1, 1, -1, -1, -1,
        -1, 1, 1, 1, -1,
        -1, 1, -1, 1, -1,
        -1, 1, 1, 1, -1,
        -1, -1, -1, -1, -1],
    seven = [1, -1, -1, -1, -1, -1,
        -1, 1, 1, 1, -1,
        -1, -1, -1, 1, -1,
        -1, -1, 1, 1, -1,
        -1, 1, 1, -1, -1,
        -1, 1, -1, -1, -1,
        -1, -1, -1, -1, -1],
    eight = [1, -1, -1, -1, -1, -1,
        -1, 1, 1, 1, -1,
        -1, 1, -1, 1, -1,
        -1, 1, 1, 1, -1,
        -1, 1, -1, 1, -1,
        -1, 1, 1, 1, -1,
        -1, -1, -1, -1, -1],
    nine = [1, -1, -1, -1, -1, -1,
        -1, 1, 1, 1, -1,
        -1, 1, -1, 1, -1,
        -1, 1, 1, 1, -1,
        -1, -1, -1, 1, -1,
        -1, 1, 1, 1, -1,
        -1, -1, -1, -1, -1];

const buttonsAmount = 36,
    examples = [
        zero,
        one,
        two,
        three,
        four,
        five,
        six,
        seven,
        eight,
        nine
    ];

(function init(examples, perceptrone, predict) {

    const perceptrones = [];
    let buttonsImage = [];

    for (let i = 0; i < buttonsAmount; i++) {
        if (i === 0)
            buttonsImage[i] = 1;
        else {
            buttonsImage[i] = -1;
            document.getElementById("grid-container")
                .insertAdjacentHTML("beforeend",
                    `<div class="grid-item" id="grid-item">${i}</div>`); // create clickable area
        }
    }
    
    for (let i = 0; i < examples.length; i++)
        perceptrones.push(perceptrone(examples, i));

    document.querySelectorAll("#grid-item.grid-item").forEach(item => {
        item.addEventListener("click", event => {

            event.currentTarget.classList.toggle("selected")

            if (event.currentTarget.classList.contains("selected")) {
                buttonsImage[event.currentTarget.innerHTML] = 1;
                document.querySelector("#prompt>#predicted").innerHTML = predict(buttonsImage, perceptrones);
            }
            else {
                buttonsImage[event.currentTarget.innerHTML] = -1;
                document.querySelector("#prompt>.predicted").innerHTML = predict(buttonsImage, perceptrones);
            }


        })
    })
})(examples, perceptrone, predict);


// functions responsible for computing 
function perceptrone(E, exampleNum) {
    let T = [], weights = [], weights_temp = [],
        living_time = 0, best_time = 0,
        chosenNum, O, ERR;

    const EPOCHS = 1000, constans = 0.1;

    for (let i = 0; i < buttonsAmount; i++) // randomizing weights balancing between -0.5 and 0.5
        weights[i] = Math.random() - 0.5;

    for (let i = 0; i < examples.length; i++)
        i === exampleNum ? T[i] = 1 : T[i] = -1;


    for (let i = 0; i < EPOCHS * 10; i++) {

        let sum = 0;
        chosenNum = Math.floor((Math.random() * 10)); // round off randomized number

        for (const j in weights)
            sum += weights[j] * E[chosenNum][j];

        sum < 0 ? O = -1 : O = 1;
        ERR = T[chosenNum] - O;
        living_time += 1;

        if (ERR !== 0) {

            living_time = 0;
            for (const j in weights) {
                weights[j] += constans * ERR * E[chosenNum][j];
            }
        }
        else if (living_time > best_time) 
            best_time = living_time;
        
    }
    return weights;
}

function predict(img, perceptrones) {
    
    for (let i = 0; i < examples.length; i++) {
        let sum = 0;

        for (const j in img) 
            sum += perceptrones[i][j] * img[j];
        
        if (sum > 0) {
            return i;
        }

    }

}